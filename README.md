# Project Two Social Media 

## Project Description

In Revature's Social Network everyone is friends with everyone else. Users can register, login to the application, and start sharing multimedia with everyone. Registered users are allowed to modify their personal information and upload their profile pictures. The application provides a search feature that allows users to search out friends and look at their profiles. Users are provided with a "feed", in which they can see what everyone is posting and like posts. Users can access and use the application via an interactive client-side single paged application that stores and retrieves multimedia using AWS S3 and consumes a RESTful web service that provides business logic and access to a database.

## Technologies Used

* Spring, Spring ORM, Spring MVC
* Java 8
* Hibernate
* HTML, CSS, JavaScript
* Log4J
* JUnit
* React, React Redux, React Router, React Spring
* TypeScript

## Features

List of features ready and TODOs for future development
* Awesome animations, loading screens and non-obtrusive notifications littered throughout the application
* Ability to upload profile and post images with AWS S3 (own credentials required)
* Full user authentication with custom backend 

To-do list:
* Refactor components to fully implement Redux
* Refactor Front-end into tsx for better reliability

## Getting Started

> git clone https://gitlab.com/AIKellar/project-two-social-media.git

- You will need your own S3 bucket and IAM user credentials to properly use the image upload functionality
- You will need your own DB credentials as well and add the appropiate env variables for the url, username, and password for your DB
- CD into the social turtle directory and run npm install
- import the backend project into maven and add apache tomcat to your targeted runtimes
- add this project to your tomcat server. To download Tomcat go here: http://tomcat.apache.org/
- Run your Tomcat server
- Run npm start in the front end. 
- You should be good to go.

## Usage

> ![](https://i.imgur.com/NMqkAon.png)

## Contributors

> @AIKellar
> @markanthonyvargas 
> @Deters
> @wstone13 
> @juuse 

